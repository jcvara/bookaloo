<?php

define('APP_DIR', dirname(__FILE__));
define('BASE_DIR', APP_DIR . '/../');
define('DB_DIR', APP_DIR . '/databases/');

define('SMARTY_BASE', BASE_DIR . 'library/smarty');
define('SMARTY_DIR', BASE_DIR . 'library/smarty/libs/');

define('BASE_URL', 'http://' . $_SERVER['SERVER_NAME']);

final class Config
{

  private static $inst = null;

  private static $configuration = array(
    'database' => array(
      'type' => 'pgsql',
      'host' => '127.0.0.1',
      'username' => 'bookaloo',
      'password' => 'bookaloo',
      'db' => 'bookaloo'
    ),
    'remember' => array(
      'cookie_name' => 'hash',
      'cookie_expiry' => 604800
    ),
    'session' => array(
      'session_name' => 'user',
      'token_name' => 'token'
    ),
    'path' => array(
      'app' => APP_DIR,
      'base' => BASE_DIR,
      'db' => DB_DIR,
      'url' => BASE_URL,
      'smarty' => array(
        'base' => SMARTY_BASE,
        'libs' => SMARTY_DIR
      )
    )
  );

  public static function getInstance()
  {

    if (self::$inst === null) {

      self::$inst = new Config();

    }

    return self::$inst;

  }

  public function get($path=NULL)
  {

    if($path) {

      $config = self::$configuration;
      $path = explode('/', $path);

      foreach ($path as $bit) {

        if(isset($config[$bit])) {

          $config = $config[$bit];

        }

      }

      return $config;

    }

    return false;

  }

}
