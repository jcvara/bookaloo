<?php

class UserController extends Controller
{

  public function login()
  {

    if(Input::exists()) {

      if(Token::check(Input::get('token'))) {

        $validator = new Validator();
        $validation = $validator->check($_POST, array(
          'username' => array('required' => true),
          'password' => array('required' => true)
        ));

        if($validation->passed()) {

          $user = $this->model('user');
          $remember = (Input::get('remember') == 'on') ? true : false;
          $login = $user->login(Input::get('username'), Input::get('password'), $remember);

          if($login) {

            Redirect::to(Config::get('path/url'));

          } else {

            Session::flash('msg', 'Nombre de usuario o contraseña incorrecto.');

          }

        } else {

          $errors = '';

          foreach ($validation->errors() as $error) {

            $errors .= $error . '<br>';

          }

          Session::flash('msg', $errors);

        }

      } else {

        Session::flash('msg', 'Sesion no valida');

      }

    }

    $this->view('home/index', []);

  }

  public function logout()
  {

    $user = $this->model('user');
    $user->logout();
    Redirect::to(Config::get('path/url'));

  }

  public function profile()
  {

  }

  public function register()
  {

    $user = $this->model('user');

    if(Input::exists()) {

      if(Token::check(Input::get('token'))) {

        $validator = new Validator();
        $validation = $validator->check($_POST, array(
          'username' => array(
            'required' => true,
            'min' => 2,
            'max' => 20,
            'unique' => 'users'
          ),
          'password' => array(
            'required' => true,
            'min' => 6
          ),
          'password_repeat' => array(
            'required' => true,
            'matches' => 'password'
          ),
          'name' => array(
            'required' => true,
            'min' => 2,
            'max' => 50
          )
        ));

        if($validation->passed()) {

          $user = new User();

          $salt = Hash::salt(32);

          try {

            $user->create(array(
              'username' => Input::get('username'),
              'password' => Hash::make(Input::get('password'), $salt),
              'salt' => $salt,
              'name' => Input::get('name'),
              'email' => Input::get('email'),
              'joined' => date('Y-m-d H:i:s'),
              'group_id' => 2
            ));

            Session::flash('msg', 'Registro completado con exito.');
            Redirect::to(Config::get('path/url'));

          } catch (Exception $e) {

            die($e->getMessage());

          }

        } else {

          $message = '';

          foreach($validation->errors() as $error) {

            $message .= $error . '<br>';

          }

          Session::flash('msg', $message);
          Redirect::to(Config::get('path/url') . '/user/register');

        }

      }

    }

    $this->view('user/register', []);

  }

  public function update()
  {

  }

}
