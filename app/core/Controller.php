<?php

class Controller
{

  public function model($model)
  {
    return new $model;
  }

  public function view($view, $data)
  {
    return new View($view, $data);
  }

}
