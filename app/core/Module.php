<?php

class Module
{

  public static function show($module, $data = [])
  {

    require $data['app'] . '/modules/' . $module . '/index.mod.php';

  }

}
