<?php

class Template
{

  public static function show($template, $data = [])
  {

    require $data['app'] . '/templates/' . $template . '.tpl.php';

  }

}
