<?php

class View
{

  public function __construct($view, $data)
  {

    $conf = Config::getInstance();

    require_once $conf->get('path/app') . '/views/' . $view . '.php';

  }

}
