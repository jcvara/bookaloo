<?php

class Input
{

  public static function exists($field = null, $type = 'post')
  {

    switch ($type) {

      case 'post':

        if(is_null($field)) {

          return (!empty($_POST)) ? true : false;

        } else {

          return (isset($_POST[$field])) ? true : false;

        }

        break;

      case 'get':

        if(is_null($field)) {

          return (!empty($_GET)) ? true : false;

        } else {

          return (isset($_GET[$field])) ? true : false;

        }

        break;

      default:

        return false;
        break;

    }

  }

  public static function get($item)
  {

    if (isset($_POST[$item])) {

      return $_POST[$item];

    } elseif (isset($_GET[$item])) {

      return $_GET[$item];

    }

  }

}
