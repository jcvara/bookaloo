<?php

class Redirect
{

  public static function to($location = null)
  {

    $conf = Config::getInstance();

    if($location) {

      if (is_numeric($location)) {

        switch ($location) {

          case 404:
            header('HTTP/1.0 404 Not Found');
            include $conf->get('path/app') . '/views/error/404.php';
            exit();
            break;

        }

      } else {

        header('Location: ' . $location);
        exit();

      }

    }

  }

}
