<?php

class Validator
{

  private $passed = false,
          $errors = [],
          $db = null;

  public function __construct()
  {
    $this->db = Database::getInstance();
  }

  public function check($source, $items = [])
  {

    foreach($items as $item => $rules) {

      foreach($rules as $rule => $rule_value) {

        $value = trim($source[$item]);
        $item = Sanitizer::escape($item);

        if($rule === 'required' && empty($value)) {

          $this->addError(trim($source[$item . '-display']) . ' no puede estar vacio.');

        } elseif(!empty($value)) {

          switch($rule) {

            case 'min':

              if(strlen($value) < $rule_value) {

                $this->addError(trim($source[$item . '-display']) . ' debe tener un minimo de ' . $rule_value . ' caracteres.');

              }

              break;

            case 'max':

              if(strlen($value) > $rule_value) {

                $this->addError(trim($source[$item . '-display']) . ' debe tener un maximo de ' . $rule_value . ' caracteres.');

              }

              break;

            case 'matches':

              if($value != $source[$rule_value]) {

                $this->addError(trim($source[$rule_value . '-display']) . ' y ' . trim($source[$item . '-display']) . ' no coinciden.');

              }

              break;

            case 'unique':

              $check = $this->db->get($rule_value, array($item, '=', $value));

              if($check->count()) {

                $this->addError(trim($source[$item . '-display']) . ' ya existe.');

              }

              break;

            default:
              //----
              break;
          }

        }

      }

    }

    if(empty($this->errors)) {

      $this->passed = true;

    }

    return $this;

  }

  private function addError($error)
  {
    $this->errors[] = $error;
  }

  public function errors()
  {
    return $this->errors;
  }

  public function passed()
  {
    return $this->passed;
  }

}
