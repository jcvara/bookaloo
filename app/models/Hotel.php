<?php

class Hotel
{

  private $db,
          $dbdata;

  public function __construct($hotel = null)
  {

    $this->db = Database::getInstance();
    $this->find($hotel);

  }

  public function create($fields = [])
  {

    if(!$this->db->insert('hotels', $fields)) {

      throw new Exception('Ha habido un problema al crear el hotel');

    }

  }

  public function data()
  {
    return $this->dbdata;
  }

  public function exists()
  {
    return (!empty($this->dbdata)) ? true : false;
  }

  public function find($hotel = null)
  {

    if($hotel) {

      $data = $this->db->get('hotels', array('id', '=', $hotel));

      if($data->count()) {

        $this->dbdata = $data->first();
        return true;

      }

    }

    return false;

  }

}
