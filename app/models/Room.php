<?php

class Room
{

  private $db,
          $dbdata;

  public function __construct($room = null)
  {

    $this->db = Database::getInstance();
    $this->find($room);

  }

  public function create($fields = [])
  {

    if(!$this->db->insert('rooms', $fields)) {

      throw new Exception('Ha habido un problema al crear la habitacion');

    }

  }

  public function data()
  {
    return $this->dbdata;
  }

  public function exists()
  {
    return (!empty($this->dbdata)) ? true : false;
  }

  public function find($room = null)
  {

    if($room) {

      $data = $this->db->get('rooms', array('id', '=', $room));

      if($data->count()) {

        $this->dbdata = $data->first();
        return true;

      }

    }

    return false;

  }

}
