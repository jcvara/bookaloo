<?php

class User
{

  private $db,
          $dbdata,
          $session,
          $cookie,
          $logged;

  public function __construct($user = null)
  {

    $this->db = Database::getInstance();
    $this->session = Config::get('session/session_name');
    $this->cookie = Config::get('remember/cookie_name');

    if(!$user) {

      if(Session::exists($this->session)) {

        $user = Session::get($this->session);

        if($this->find($user)) {

          $this->logged = true;

        } else {
          //logged out
        }

      }

    } else {

      $this->find($user);

    }

  }

  public function create($fields = [])
  {

    if(!$this->db->insert('users', $fields)) {

      return false;

    }

    return true;

  }

  public function delete($id = null)
  {

    if(!$this->db->delete('users', array('id', '=', $id))) {

      return false;

    }

    return true;

  }


  public function data()
  {
    return $this->dbdata;
  }

  public function exists()
  {
    return (!empty($this->dbdata)) ? true : false;
  }

  public function find($user = null)
  {

    if($user) {

      $field = (is_numeric($user)) ? 'id' : 'username';
      //TODO: Need to validate usernames on login to forbid only numbers

      $data = $this->db->get('users', array($field, '=', $user));

      if($data->count()) {

        $this->dbdata = $data->first();
        return true;

      }

    }

    return false;

  }

  public function isLogged()
  {
    return $this->logged;
  }

  public function login($username = null, $password = null, $remember = true)
  {

    if (!$username && !$password && $this->exists()) {

      Session::put($this->session, $this->data()->id);

    } else {

      $user = $this->find($username);

      if($user) {

        if($this->data()->password === Hash::make($password, $this->data()->salt)) {

          Session::put($this->session, $this->data()->id);

          if($remember) {

            $hashCheck = $this->db->get('users_session', array('user_id', '=', $this->data()->id));

            if(!$hashCheck->count()) {

              $hash = Hash::unique();

              $this->db->insert('users_session', array(
                'user_id' => $this->data()->id,
                'hash' => $hash
              ));

            } else {

              $hash = $hashCheck->first()->hash;

            }

            Cookie::put($this->cookie, $hash, Config::get('remember/cookie_expiry'));

          }

          return true;

        }

      }

    }

    return false;

  }

  public function logout()
  {

    $this->db->delete('users_session', array('user_id', '=', $this->data()->id));

    Session::delete($this->session);
    Cookie::delete($this->cookie);

  }

}
