<?php

$db = Database::getInstance();
$db->query('SELECT * FROM groups', []);

$groups = $db->results();

?>
<table>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Permisos</th>
    </tr>
  </thead>
  <tbody>
  <?php
    foreach ($groups as $group) {
      echo '<tr>';
      echo '<td>' . $group->id . '</td>';
      echo '<td>' . $group->name . '</td>';
      echo '<td>' . $group->permissions . '</td>';
      echo '</tr>';
    }
  ?>
  </tbody>
</table>
