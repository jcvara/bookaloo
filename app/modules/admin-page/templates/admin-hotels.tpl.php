<?php

$db = Database::getInstance();
$db->query('SELECT * FROM users', []);

$users = $db->results();

$db->query('SELECT * FROM hotels', []);

$hotels = $db->results();

?>
<table>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Direccion</th>
      <th scope="col">Pais</th>
      <th scope="col">Ciudad</th>
      <th scope="col">Fecha de alta</th>
      <th scope="col">Usuario</th>
    </tr>
  </thead>
  <tbody>
  <?php
    foreach ($hotels as $hotel) {
      echo '<tr>';
      echo '<td>' . $hotel->id . '</td>';
      echo '<td>' . $hotel->name . '</td>';
      echo '<td>' . $hotel->address . '</td>';
      echo '<td>' . $hotel->country . '</td>';
      echo '<td>' . $hotel->city . '</td>';
      echo '<td>' . $hotel->added . '</td>';
      echo '<td>';
      foreach ($users as $user) {
        if ($user->id === $hotel->user_id) {
          echo $user->username;
          break;
        }
      }
      echo '</td>';
      echo '</tr>';
    }
  ?>
  </tbody>
</table>
