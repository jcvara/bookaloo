<?php

$db = Database::getInstance();
$db->query('SELECT * FROM rooms', []);

$rooms = $db->results();

$db->query('SELECT * FROM hotels', []);

$hotels = $db->results();

?>
<table>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Numero</th>
      <th scope="col">Plazas</th>
      <th scope="col">Baño</th>
      <th scope="col">Bed & Breakfast</th>
      <th scope="col">Pension Completa</th>
      <th scope="col">Fecha de alta</th>
      <th scope="col">Hotel</th>
    </tr>
  </thead>
  <tbody>
  <?php
    foreach ($rooms as $room) {
      echo '<tr>';
      echo '<td>' . $room->id . '</td>';
      echo '<td>' . $room->number . '</td>';
      echo '<td>' . $room->vacancies . '</td>';
      echo '<td>' . ($room->bathroom ? 'Si' : 'No') . '</td>';
      echo '<td>' . $room->price_bb . '</td>';
      echo '<td>' . $room->price_full . '</td>';
      echo '<td>' . $room->added . '</td>';
      echo '<td>';
      foreach ($hotels as $hotel) {
        if ($hotel->id === $room->hotel_id) {
          echo $hotel->name;
          break;
        }
      }
      echo '</td>';
      echo '</tr>';
    }
  ?>
  </tbody>
</table>
