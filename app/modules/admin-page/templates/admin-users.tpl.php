<?php

$db = Database::getInstance();
$db->query('SELECT * FROM users', []);

$users = $db->results();

$db->query('SELECT * FROM groups', []);

$groups = $db->results();

?>
<table>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre de usuario</th>
      <th scope="col">Nombre</th>
      <th scope="col">E-mail</th>
      <th scope="col">Fecha de alta</th>
      <th scope="col">Grupo</th>
    </tr>
  </thead>
  <tbody>
  <?php
    foreach ($users as $user) {
      echo '<tr>';
      echo '<td>' . $user->id . '</td>';
      echo '<td>' . $user->username . '</td>';
      echo '<td>' . $user->name . '</td>';
      echo '<td>' . $user->email . '</td>';
      echo '<td>' . date('Y-m-d H:i', strtotime($user->joined)) . '</td>';
      echo '<td>';
      foreach ($groups as $group) {
        if ($group->id === $user->group_id) {
          echo $group->name;
          break;
        }
      }
      echo '</td>';
      echo '</tr>';
    }
  ?>
  </tbody>
</table>

<form class="admin-buttons" method="post">
  <input type="hidden" id="user-id" name="user-id" value="none">
  <button id="button-create" name="action" value="user-create">Nuevo</button>
  <button id="button-update" name="action" value="user-update">Actualizar</button>
  <button id="button-delete" name="action" value="user-delete">Borrar</button>
</form>

<form method="post">

</form>
