<footer>

  <a class="license" rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">
    <img alt="Creative Commons License" src="assets/img/cc-by-nc-sa.png">
  </a>

  <p>Trabajo bajo licencia <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es_ES">Creative Commons BY-NC-SA 3.0 Unported</a> por <a href="https://bitbucket.org/jcvara">Juan Carlos Vara Perez</a>.</p>

</footer>
