<?php

if($data['logged']) {

  include('templates/session.tpl.php');

} else {

  include('templates/login.tpl.php');

}
