<div class="user-session">Login

  <div class="user-popup">

    <div class="user-popup-inner">

      <form action="/user/login" method="post">

        <div class="login-field">

          <label for="username">Usuario</label>
          <input type="text" name="username" autocomplete="off">
          <input type="hidden" name="username-display" value="Nombre de Usuario">

        </div>

        <div class="login-field">

          <label for="password">Contraseña</label>
          <input type="password" name="password" autocomplete="off">
          <input type="hidden" name="password-display" value="Contraseña">

        </div>

        <div class="login-field">

          <label for="remember">
            <input type="checkbox" name="remember" id="remember">Recordar sesion
          </label>

        </div>

        <p class="register-link">¿No tienes cuenta? <a href="/user/register">¡Registrate!</a></p>

        <input type="hidden" name="token" value="<?=$data['token']?>">

        <button type="submit" class="button-login">Entrar</button>

      </form>

    </div>

  </div>

</div>
