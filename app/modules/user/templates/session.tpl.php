<?php

$user = new User();

?>
<div class="user-session"><?=$data['username']?>

  <div class="user-popup">

    <div class="user-popup-inner">

      <ul>

        <?= $user->data()->group_id === 1 ? '<li><a href="/administration">Administracion</a></li>' : '' ; ?>
        <li><a href="/user/logout">Salir</a></li>

      </ul>

    </div>

  </div>

</div>
