<?php

if (session_status() == PHP_SESSION_NONE) {

  session_start();

}

require_once('config.php');

spl_autoload_register(function($class)
{

  $appPath = Config::get('path/app');

  $file = ucfirst($class) . '.php';

  if (file_exists($appPath . '/core/' . $file)) {

    require_once $appPath . '/core/' . $file;

  } elseif (file_exists($appPath . '/helpers/' . $file)) {

    require_once $appPath . '/helpers/' . $file;

  } elseif (file_exists($appPath . '/models/' . $file)) {

    require_once $appPath . '/models/' . $file;

  }

});

if(Cookie::exists(Config::get('remember/cookie_name')) && !Session::exists(Config::get('session/session_name'))) {

  $hash = Cookie::get(Config::get('remember/cookie_name'));
  $hashCheck = Database::getInstance()->get('users_session', array('hash', '=', $hash));

  if($hashCheck->count()) {

    $user = new User($hashCheck->first()->user_id);
    $user->login();

  }

}
