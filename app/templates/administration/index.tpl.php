<?php

$data['page'] = (Input::exists('page')) ? Input::get('page') : 'admin-users';

if (Input::exists('action')) {

  if (Input::get('action') === 'user-delete') {

    $user = new User();

    if($user->delete(Input::get('user-id'))) {

      Session::flash('msg', 'Usuario borrado con exito');

    } else {

      Session::flash('msg', 'Ha habido un problema al borrar el usuario');

    }

    Redirect::to('administration/');

  }

}

?>

<!DOCTYPE html>
<html lang="es">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Buscador de hoteles Low-Cost - Bookaloo</title>

    <link href="<?=$data['base']?>/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$data['base']?>/assets/css/common.css" rel="stylesheet">
    <link href="<?=$data['base']?>/assets/css/administration.css" rel="stylesheet">

  </head>

  <body>

    <div class="body-wrapper">

      <?=Module::show('header', $data)?>

      <main>

        <?php if($data['message']) {Module::show('message', $data);} ?>

        <div class="admin-menu">

        <?=Module::show('admin-menu', $data)?>

        </div><!-- #controls -->

        <section class="admin-main">

        <?=Module::show('admin-page', $data);?>

        </section>

      </main>

    </div><!-- #body-wrapper-->

    <?=Module::show('footer', $data)?>

    <script src="<?=$data['base']?>/vendor/jquery/jquery-2.1.1.min.js"></script>
    <script src="<?=$data['base']?>/assets/js/administration.js"></script>

  </body>

</html>
