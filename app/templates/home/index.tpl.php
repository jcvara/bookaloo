<!DOCTYPE html>
<html lang="es">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Buscador de hoteles Low-Cost - Bookaloo</title>

    <link href="<?=$data['base']?>/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$data['base']?>/assets/css/common.css" rel="stylesheet">
    <link href="<?=$data['base']?>/assets/css/carousel.css" rel="stylesheet">
    <link href="<?=$data['base']?>/vendor/jquery/plugins/calendarPicker/jquery.calendarPicker.css" rel="stylesheet">

  </head>

  <body>

    <div class="body-wrapper">

      <?=Module::show('header', $data)?>

      <div class="jcarousel-wrapper">

        <div class="jcarousel">

          <ul>
            <li><img src="assets/img/travel1cr.jpg" width="970" height="300" alt="Viajes 1"></li>
            <li><img src="assets/img/travel2cr.jpg" width="970" height="300" alt="Viajes 2"></li>
            <li><img src="assets/img/travel3cr.jpg" width="970" height="300" alt="Viajes 3"></li>
            <li><img src="assets/img/travel4cr.jpg" width="970" height="300" alt="Viajes 4"></li>
          </ul>

        </div>

        <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
        <a href="#" class="jcarousel-control-next">&rsaquo;</a>
        <!-- <p class="jcarousel-pagination"></p> -->

      </div>

      <!--<div class="jumbo">

        <p class="jumbo-text">Encuentra tu hotel ideal</p>

      </div> #jumbotron -->

      <main>

        <?php if($data['message']) {Module::show('message', $data);} ?>

        <section class="listings">

        <?=Module::show('listings', $data)?>

        </section>

        <div class="controls">

        <?=Module::show('controls', $data)?>

        </div><!-- #controls -->

      </main>

    </div><!-- #body-wrapper-->

    <?=Module::show('footer', $data)?>

    <script src="<?=$data['base']?>/vendor/jquery/jquery-2.1.1.min.js"></script>
    <script src="<?=$data['base']?>/vendor/jquery/plugins/jcarousel/jquery.jcarousel.min.js"></script>
    <script src="<?=$data['base']?>/assets/js/carousel.js"></script>
    <script src="<?=$data['base']?>/vendor/jquery/plugins/calendarPicker/jquery.calendarPicker.js"></script>
    <script src="<?=$data['base']?>/vendor/jquery/plugins/calendarPicker/test/jquery.mousewheel.min.js"></script>
    <script src="<?=$data['base']?>/assets/js/calendar.js"></script>

  </body>

</html>
