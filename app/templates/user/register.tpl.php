<!DOCTYPE html>
<html lang="es">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Buscador de hoteles Low-Cost - Bookaloo</title>

    <link href="<?=$data['base']?>/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$data['base']?>/assets/css/common.css" rel="stylesheet">
    <link href="<?=$data['base']?>/assets/css/register.css" rel="stylesheet">

  </head>

  <body>

    <div class="body-wrapper">

      <?=Module::show('header', $data)?>

      <div class="jumbo">

        <p class="jumbo-text">Encuentra tu hotel ideal</p>

      </div><!-- #jumbotron -->

      <?php if($data['message']) {Module::show('message', $data);} ?>

      <div class="registration-form">

        <form action="" method="post" accept-charset="UTF-8">

          <div class="register-field">
            <label for="username">Usuario</label>
            <input type="text" name="username" id="username" value="<?=Sanitizer::escape(Input::get('username'))?>" placeholder="Usuario" autocomplete="off">
            <input type="hidden" name="username-display" value="Nombre de Usuariohttp://bookaloo.local/administration">
          </div>

          <div class="register-field">
            <label for="password">Contraseña</label>
            <input type="password" name="password" id="password" placeholder="Contraseña">
            <input type="hidden" name="password-display" value="Contraseña">
          </div>

          <div class="register-field">
            <label for="password_repeat">Repetir Contraseña</label>
            <input type="password" name="password_repeat" id="password_repeat" placeholder="Repetir Contraseña">
            <input type="hidden" name="password_repeat-display" value="Repetir Contraseña">
          </div>

          <div class="register-field">
            <label for="name">Nombre</label>
            <input type="text" name="name" id="name" value="<?=Sanitizer::escape(Input::get('name'))?>" placeholder="Nombre" autocomplete="off">
            <input type="hidden" name="name-display" value="Nombre">
          </div>

          <input type="hidden" name="token" value="<?=Token::generate()?>">
          <button type="submit" class="button-register">Registrarse</button>

        </form>

      </div>

    </div><!-- #body-wrapper-->

    <?=Module::show('footer', $data)?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=$data['base']?>/vendor/jquery/jquery-2.1.1.min.js"></script>

  </body>

</html>
