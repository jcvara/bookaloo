<?php

$conf = Config::getInstance();

$data['app'] = $conf->get('path/app');
$data['base'] = $conf->get('path/url');
$data['message'] = Session::exists('msg');

$data['token'] = Token::generate();

if($data['message']) {
  $data['flash'] = Session::flash('msg');
}

$user = new User();

if($user->isLogged() && $user->data()->group_id === 1) {

  $data['logged'] = true;
  $data['username'] = ucfirst(Sanitizer::escape($user->data()->username));
  Template::show('administration/index', $data);

} else {

  Session::flash('msg', 'Acceso denegado');
  Redirect::to($data['base']);

}

