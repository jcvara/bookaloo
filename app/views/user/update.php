<?php

$conf = Config::getInstance();

$data['app'] = $conf->get('path/app');
$data['base'] = $conf->get('path/url');
$data['message'] = Session::exists('msg');

$data['token'] = Token::generate();

if($data['message']) {
  $data['flash'] = Session::flash('msg');
}

$user = new User();

if($user->isLogged()) {

  $data['logged'] = true;
  $data['username'] = ucfirst(Sanitizer::escape($user->data()->username));

} else {

  $data['logged'] = false;

}

Template::show('user/update', $data);
