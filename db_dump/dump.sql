--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE bookaloo;
ALTER ROLE bookaloo WITH SUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md561dee6ffb774f1f61134403b599334fc';
CREATE ROLE mankeulv;
ALTER ROLE mankeulv WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN NOREPLICATION;
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION PASSWORD 'md533a7b088baee0ff60f29c5ac83d2255a';






--
-- Database creation
--

CREATE DATABASE bookaloo WITH TEMPLATE = template0 OWNER = bookaloo;
REVOKE ALL ON DATABASE template1 FROM PUBLIC;
REVOKE ALL ON DATABASE template1 FROM postgres;
GRANT ALL ON DATABASE template1 TO postgres;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


\connect bookaloo

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: Bookings_id_seq; Type: SEQUENCE; Schema: public; Owner: bookaloo
--

CREATE SEQUENCE "Bookings_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Bookings_id_seq" OWNER TO bookaloo;

--
-- Name: Groups_id_seq; Type: SEQUENCE; Schema: public; Owner: bookaloo
--

CREATE SEQUENCE "Groups_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Groups_id_seq" OWNER TO bookaloo;

--
-- Name: Hotels_id_seq; Type: SEQUENCE; Schema: public; Owner: bookaloo
--

CREATE SEQUENCE "Hotels_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Hotels_id_seq" OWNER TO bookaloo;

--
-- Name: Rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: bookaloo
--

CREATE SEQUENCE "Rooms_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Rooms_id_seq" OWNER TO bookaloo;

--
-- Name: Sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: bookaloo
--

CREATE SEQUENCE "Sessions_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Sessions_id_seq" OWNER TO bookaloo;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: users; Type: TABLE; Schema: public; Owner: bookaloo; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(20),
    password character varying(64),
    name character varying(60),
    email character varying(50),
    joined timestamp with time zone DEFAULT now(),
    group_id integer DEFAULT 1 NOT NULL,
    salt character varying(32)
);


ALTER TABLE public.users OWNER TO bookaloo;

--
-- Name: Users_id_seq; Type: SEQUENCE; Schema: public; Owner: bookaloo
--

CREATE SEQUENCE "Users_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Users_id_seq" OWNER TO bookaloo;

--
-- Name: Users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bookaloo
--

ALTER SEQUENCE "Users_id_seq" OWNED BY users.id;


--
-- Name: bookings; Type: TABLE; Schema: public; Owner: bookaloo; Tablespace: 
--

CREATE TABLE bookings (
    id integer DEFAULT nextval('"Bookings_id_seq"'::regclass) NOT NULL,
    checkin timestamp with time zone,
    checkout timestamp with time zone,
    bb_full boolean,
    added timestamp with time zone,
    user_id integer NOT NULL,
    room_id integer NOT NULL
);


ALTER TABLE public.bookings OWNER TO bookaloo;

--
-- Name: groups; Type: TABLE; Schema: public; Owner: bookaloo; Tablespace: 
--

CREATE TABLE groups (
    id integer DEFAULT nextval('"Groups_id_seq"'::regclass) NOT NULL,
    name character varying(20),
    permissions text
);


ALTER TABLE public.groups OWNER TO bookaloo;

--
-- Name: hotels; Type: TABLE; Schema: public; Owner: bookaloo; Tablespace: 
--

CREATE TABLE hotels (
    id integer DEFAULT nextval('"Hotels_id_seq"'::regclass) NOT NULL,
    name character varying(50),
    address character varying(100),
    country character varying(50),
    state character varying(50),
    city character varying(50),
    picture character varying,
    added timestamp with time zone,
    user_id integer NOT NULL
);


ALTER TABLE public.hotels OWNER TO bookaloo;

--
-- Name: rooms; Type: TABLE; Schema: public; Owner: bookaloo; Tablespace: 
--

CREATE TABLE rooms (
    id integer DEFAULT nextval('"Rooms_id_seq"'::regclass) NOT NULL,
    number integer,
    vacancies integer,
    bathroom boolean,
    price_bb money,
    price_full money,
    picture character varying,
    added timestamp with time zone,
    hotel_id integer NOT NULL
);


ALTER TABLE public.rooms OWNER TO bookaloo;

--
-- Name: users_session; Type: TABLE; Schema: public; Owner: bookaloo; Tablespace: 
--

CREATE TABLE users_session (
    id integer DEFAULT nextval('"Sessions_id_seq"'::regclass) NOT NULL,
    hash character varying(50),
    user_id integer
);


ALTER TABLE public.users_session OWNER TO bookaloo;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bookaloo
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('"Users_id_seq"'::regclass);


--
-- Name: Bookings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bookaloo
--

SELECT pg_catalog.setval('"Bookings_id_seq"', 1, false);


--
-- Name: Groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bookaloo
--

SELECT pg_catalog.setval('"Groups_id_seq"', 2, true);


--
-- Name: Hotels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bookaloo
--

SELECT pg_catalog.setval('"Hotels_id_seq"', 1, false);


--
-- Name: Rooms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bookaloo
--

SELECT pg_catalog.setval('"Rooms_id_seq"', 1, false);


--
-- Name: Sessions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bookaloo
--

SELECT pg_catalog.setval('"Sessions_id_seq"', 1, false);


--
-- Name: Users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bookaloo
--

SELECT pg_catalog.setval('"Users_id_seq"', 7, true);


--
-- Data for Name: bookings; Type: TABLE DATA; Schema: public; Owner: bookaloo
--



--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: bookaloo
--

INSERT INTO groups VALUES (1, 'Standard User', '');
INSERT INTO groups VALUES (2, 'Administrator', '{"admin": 1}');


--
-- Data for Name: hotels; Type: TABLE DATA; Schema: public; Owner: bookaloo
--



--
-- Data for Name: rooms; Type: TABLE DATA; Schema: public; Owner: bookaloo
--



--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: bookaloo
--

INSERT INTO users VALUES (5, 'Alex', '6ced4f5065d00acd2f4360fd106b7ea50654c1ee456526e56f2b1f7b5ccd4196', 'Alex Garrett', '', '2014-11-03 19:50:54+01', 1, '???xf?????|?n??????4L???');
INSERT INTO users VALUES (6, 'sdfghsefghsadf', 'e5711a4bdb449184535b4f494a6309ba1e1b710f9d9c5a4f4c12a65163142098', 'edfsgsdfghsd', '', '2014-11-03 20:01:35+01', 1, '?J?L??e?J-?6B?k''?3');
INSERT INTO users VALUES (7, 'sdfgefgsdfg', '55b0d2c4c0d53ebefe08a3f922459d9d4f6f7c1f794bd7e5faf471ad73186f26', 'Alex Garrett', '', '2014-11-03 20:10:04+01', 1, '??e?H??Ap5?9 9,?U?Gt??~?;');


--
-- Data for Name: users_session; Type: TABLE DATA; Schema: public; Owner: bookaloo
--



--
-- Name: Bookings_pkey; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT "Bookings_pkey" PRIMARY KEY (id);


--
-- Name: Groups_pkey; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT "Groups_pkey" PRIMARY KEY (id);


--
-- Name: Hotels_pkey; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY hotels
    ADD CONSTRAINT "Hotels_pkey" PRIMARY KEY (id);


--
-- Name: Rooms_id_key; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY rooms
    ADD CONSTRAINT "Rooms_id_key" UNIQUE (id);


--
-- Name: Rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY rooms
    ADD CONSTRAINT "Rooms_pkey" PRIMARY KEY (id);


--
-- Name: Users_id_key; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "Users_id_key" UNIQUE (id);


--
-- Name: Users_pkey; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id);


--
-- Name: bookings_id_key; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT bookings_id_key UNIQUE (id);


--
-- Name: groups_id_key; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_id_key UNIQUE (id);


--
-- Name: hotels_id_key; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY hotels
    ADD CONSTRAINT hotels_id_key UNIQUE (id);


--
-- Name: users_session_id_key; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY users_session
    ADD CONSTRAINT users_session_id_key UNIQUE (id);


--
-- Name: users_session_pkey; Type: CONSTRAINT; Schema: public; Owner: bookaloo; Tablespace: 
--

ALTER TABLE ONLY users_session
    ADD CONSTRAINT users_session_pkey PRIMARY KEY (id);


--
-- Name: Bookings_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bookaloo
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT "Bookings_room_id_fkey" FOREIGN KEY (room_id) REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Bookings_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bookaloo
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT "Bookings_user_id_fkey" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Hotels_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bookaloo
--

ALTER TABLE ONLY hotels
    ADD CONSTRAINT "Hotels_user_id_fkey" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Rooms_hotel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bookaloo
--

ALTER TABLE ONLY rooms
    ADD CONSTRAINT "Rooms_hotel_id_fkey" FOREIGN KEY (hotel_id) REFERENCES hotels(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Users_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bookaloo
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "Users_group_id_fkey" FOREIGN KEY (group_id) REFERENCES groups(id) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

