$(document).ready(function() {

  $("table tr").click(function() {

     $(this).addClass("selected-row").siblings().removeClass("selected-row").addClass("blurred-row");

     $("input[name=user-id]").val($(this).find("td:first").html());

  });

  $(".admin-buttons").on("submit", function(e) {

      if($("input[name=user-id]").val() === "none") {
        window.alert('No hay ninguna fila seleccionada');
        e.preventDefault();
      }

  });

});
