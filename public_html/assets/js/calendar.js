(function($) {

  $(function() {

    var checkin,
        checkout;

    var cal = $("#calendar").calendarPicker({
      monthNames:["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
      dayNames: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
      //useWheel:true,
      //callbackDelay:500,
      years:0,
      months:1,
      days:1,
      showDayArrows:false
    });

    $("#calendar-select").click(function(){

        if ($("#calendar-stage").val() == 1) {

          checkin = cal.currentDate.getDate() + "-" + (cal.currentDate.getMonth() + 1) + "-" + cal.currentDate.getFullYear();
          $("#calendar-prompt").html("Check out");
          $("#calendar-stage").val(2);

        } else {

          checkout = cal.currentDate.getDate() + "-" + (cal.currentDate.getMonth() + 1) + "-" + cal.currentDate.getFullYear();
          $("#calendar-prompt").html("Check in");
          $("#calendar-stage").val(1);

          window.alert("Checkin: " + checkin + "\nCheckout: " + checkout);

        }

    });

  });

})(jQuery);
